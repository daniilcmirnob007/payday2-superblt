# Version 3.3.5

* Hotfix for missing function signature - Hoppip
* Update OpenSSL, ZLib and OGG decoder - test1
* Implement SystemFS:delete_file - Der Muggemann
* Disable HRTF to improve audio quality - test1

# Version 3.3.4

* Improve Linux specific code - ZNix, Azer0s, whitequark
* Improve XML parsing - Hoppip
* Update Lua function signature - Cpone

# Version 3.3.3

* Fix AssetDB Idstring parsing - Hoppip
* Improve Linux specific code - CraftedCart
* Update to a less strict, custom version of mxml - Hoppip
* Reduce amount of http request logging - Hoppip

# Version 3.3.2

This is a hotfix - if your game already works fine, upgrading to this version won't change anything.

* Fix crashes on computers with all.blb or any other files (not folders) with names shorter than eight letters in the assets directory - ZNix
* Fix a compile issue on Linux - roberChen

# Version 3.3.1 and below

These versions do not have nicely formatted changelogs, but you can see their Git commit history in GitLab if required.
